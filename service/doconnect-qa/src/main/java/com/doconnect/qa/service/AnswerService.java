package com.doconnect.qa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.doconnect.qa.model.Answer;
import com.doconnect.qa.model.Question;
import com.doconnect.qa.repository.AnswerRepository;

@Service
public class AnswerService {
	@Autowired
    private AnswerRepository answerRepository;
	
	public void saveAnswer(Answer a) {
        answerRepository.save(a);
    }

	public List<Answer> getAll() {
		return answerRepository.findAll();
	}

	public Answer getById(int id) {
		
		return answerRepository.findById(id).isPresent() ? answerRepository.findById(id).get() : new Answer();
	}

	public List<Answer> getByTopic(String topic) {
		
		return answerRepository.getAnswersByTopic(topic);
	}

}
